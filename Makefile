.PHONY: build major minor patch

# packer:
# 	packer=$$(which packer) \
# 	if [[ ! -e $$packer ]]; \
# 	then; \
# 		echo "" \
# 		exit 1 \
# 	fi

build:
	#ansible-galaxy install -r requirements.yml --ignore-errors
	packer build -var-file=vars.json -var version=$$(cat version) docker-ce.ami.json

major:
	cat version | xargs ./bin/increment_version.sh -M > temp
	mv temp version
	make build

minor:
	cat version | xargs ./bin/increment_version.sh -m > temp
	mv temp version
	make build

patch:
	cat version | xargs ./bin/increment_version.sh -p > temp
	mv temp version
	make build

